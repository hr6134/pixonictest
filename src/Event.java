import java.time.LocalDateTime;
import java.util.concurrent.Callable;

/**
 * Comparable by time event entity (earlier events considered as greater ones) with callback.
 *
 * @author hr6134
 *         11.12.17
 */
public class Event<T> implements Comparable<Event<T>> {
    private final LocalDateTime date;
    private final Callable<T> callable;

    public Event(LocalDateTime date, Callable<T> callable) {
        this.date = date;
        this.callable = callable;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public Callable<T> getCallable() {
        return callable;
    }

    @Override
    public int compareTo(Event<T> o) {
        // since we are prioritizing earlier events, default comparator should be multiply by -1
        return  -1 * o.getDate().compareTo(this.date);
    }
}
