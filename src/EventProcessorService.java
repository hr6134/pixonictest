import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Random;
import java.util.concurrent.*;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Service that could concurrently process input events at specified time.
 *
 * @author hr6134
 *         11.12.17
 */
public class EventProcessorService<T> {
    private final PriorityBlockingQueue<Event<T>> queue = new PriorityBlockingQueue<>();

    private final Lock lock = new ReentrantLock();
    private final Condition alarmCondition = lock.newCondition();
    private final Condition newEventCondition = lock.newCondition();

    private final ExecutorService executor;

    public EventProcessorService(ExecutorService executor) {
        this.executor = executor;
    }

    /**
     * Test performance for two sparse events.
     *
     * @param eventProcessorService
     */
    public static void twoSparseEvents(EventProcessorService<String> eventProcessorService) {
        LocalDateTime now15 = LocalDateTime.now().plusSeconds(15);
        LocalDateTime now30 = LocalDateTime.now().plusSeconds(30);
        eventProcessorService.push(now30, () -> {
            System.out.println(now30);
            return now30.toString();
        });

        try {
            Thread.sleep(1000L * 10L); // sleep for 10 seconds
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        eventProcessorService.push(now15, () -> {
            System.out.println(now15);
            return now15.toString();
        });
    }

    /**
     * Busy loop which push events into the service with random delay within 10 seconds.
     *
     * @param eventProcessorService
     */
    public static void randomEvents(EventProcessorService<String> eventProcessorService) {
        // thread that will write to the queue for the test purpose
        new Thread(() -> {
            while (true) {
                // add event with random timestamp within 10 seconds from now
                LocalDateTime now = LocalDateTime.now().plusSeconds(new Random().nextInt(10));
                eventProcessorService.push(now, () -> {
                    System.out.println(now);
                    return now.toString();
                });

                try {
                    Thread.sleep(500L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public static void main(String[] args) {
        EventProcessorService<String> eventProcessorService = new EventProcessorService<>(
                Executors.newFixedThreadPool(8)
        );
        eventProcessorService.start();

        twoSparseEvents(eventProcessorService);
//        randomEvents(eventProcessorService);
    }

    /**
     * Start processing thread for input events.
     */
    public void start() {
        // task that will read from the queue
        executor.submit(() -> {
            // we don't worry about busy loop, because of await call in queue.take()
            while (true) {
                try {
                    Event<T> event = queue.take();
                    LocalDateTime now = LocalDateTime.now();
                    // todo consider: depend on the task it could be a good idea to reject too old events
                    if (event.date.isBefore(now) || event.date.isEqual(now)) {
                        executor.submit(event.callable);
                    } else {
                        queue.add(event);

                        long delta = now.until(event.date, ChronoUnit.MILLIS);
                        // start an alarm thread
                        new Thread(() -> {
                            try {
                                lock.lock();
                                newEventCondition.await(delta, TimeUnit.MILLISECONDS);
                                lock.unlock();

                                lock.lock();
                                alarmCondition.signal();
                                lock.unlock();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }).start();

                        // wait for alarm or new event
                        lock.lock();
                        alarmCondition.await();
                        lock.unlock();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void push(LocalDateTime date, Callable<T> callable) {
        queue.offer(new Event<>(date, callable), 1000, TimeUnit.MILLISECONDS);

        lock.lock();
        newEventCondition.signal();
        lock.unlock();
    }

    private static class Event<T> implements Comparable<Event<T>> {
        final LocalDateTime date;
        final Callable<T> callable;
        final long createdTimestamp;

        Event(LocalDateTime date, Callable<T> callable) {
            this.date = date;
            this.callable = callable;
            this.createdTimestamp = System.currentTimeMillis();
        }

        @Override
        public int compareTo(Event<T> o) {
            int r = this.date.compareTo(o.date);
            if (r == 0) return Long.compare(this.createdTimestamp, o.createdTimestamp);
            return  r;
        }

        @Override
        public String toString() {
            return "Event { " +
                    "date = " + date +
                    ", createdTimestamp = " + createdTimestamp +
                    " }";
        }
    }
}
